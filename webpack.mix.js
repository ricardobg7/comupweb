const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/sass/frontend/main.scss', 'public/css/frontend/style.css', {
	outputStyle: 'compressed'
})

mix.js('resources/js/frontend/style.js', 'public/js/frontend/style.js');


