<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('frontend.pages.home');
});


Route::get('/about', function () {
	return view('frontend.pages.about');
});

Route::get('/services', function () {
	return view('frontend.pages.services');
});


Route::get('/blog', function () {
	return view('frontend.pages.blog');
});


Route::get('/blog/post', function () {
	return view('frontend.pages.post');
});

Route::get('/contacts', function () {
	return view('frontend.pages.contacts');
});
