<nav class="navbar is-spaced" role="navigation" aria-label="main navigation">
	<div class="navbar-brand">
		<a class="navbar-item" href="/">
			<img src="{{ asset('images/brand/brand-logo.png') }}">
		</a>
		<a id="burger-menu" role="button" class="navbar-burger burger is-vcentered" aria-label="menu"
			aria-expanded="false" data-target="navbar">
			<span aria-hidden="true"></span>
			<span aria-hidden="true"></span>
			<span aria-hidden="true"></span>
		</a>
	</div>
	<div id="navbar" class="navbar-menu">
		<div class="navbar-end">
			<a class="navbar-item" href="/">
				Home
			</a>
			<a class="navbar-item" href="/about">
				About
			</a>
			<a class="navbar-item" href="/services">
				Services
			</a>
			<a class="navbar-item" href="#">
				Portfolio
			</a>
			<a class="navbar-item" href="/blog">
				Blog
			</a>
			<a class="navbar-item" href="/contacts">
				Contacts
			</a>
		</div>

		<div class="navbar-languages">
			<a id="pt" class="navbar-item" href="/">
				PT
			</a>
			<p class="navbar-item">|</p>
			<a id="en" class="navbar-item" href="/about">
				EN
			</a>

		</div>
	</div>
</nav>