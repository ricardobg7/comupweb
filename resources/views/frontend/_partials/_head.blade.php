<head>
	<!-- ignorar -->
	<meta property="og:url" content="https://comup.pt" />
	<meta property="og:title" content="Comup | #TOTHETOP" />
	<meta property="og:type" content="website" />
	<meta property="og:description" content="Comup Web Agency." />
	<meta property="og:image" content="{{asset('/img/logo.png')}}" />


	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">

	<!-- mudar font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,600i,700,800,800i" rel="stylesheet">

	<link rel="stylesheet" href="{{ asset('css/frontend/style.css') }}">

	<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.css"> -->
	<link rel="shortcut icon" type="image/png" href="{{ asset('img/favicon.png') }}">

	<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript"
		src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js">
	</script>
	<script src="{{ asset('js/frontend/style.js') }}"></script>

	<!-- SWIPER Library -->

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.min.js"></script>

	<!-- AOS Library + library start up -->
	<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
	<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

	<title>Comup Agency</title>
</head>