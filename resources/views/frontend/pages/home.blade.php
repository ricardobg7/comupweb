@extends('frontend.main')

@section('content')

<section class="hero is-info is-large">
	<div class="hero-body">

		<div id="flow">
			<span class="flow-1"></span>
			<span class="flow-2"></span>
			<span class="flow-3"></span>
		</div>
		<div class="container">
			<h1 class="title has-text-black is-size-1" data-aos="fade-up">
				Comup Agency
			</h1>
			<h2 class="subtitle has-text-black is-size-3" data-aos="fade-up">
				TO THE TOP
			</h2>
		</div>

	</div>
</section>


<section class="section section__expertise ls-features-cards">
	<div class="container">
		<div class="columns">
			<div class="column is-8-desktop is-offset-2-desktop">
				<div class="ls-section-content has-text-centered">
					<h2 class="title is-2 is-uppercase has-text-dark">What we are good at</h2>
					<p class="content has-text-grey is-size-5">Small description - Capitalize on low hanging fruit to
						identify a ballpark value added activity to beta test. Override the digital divide
						with additional clickthroughs.
					</p>
				</div>
			</div>
		</div>
		<div class="columns">
			<div class="column is-one-third">
				<article class="card ls-feature-card" data-aos="flip-left" data-aos-delay="0" data-aos-duration="750"
					data-aos-easing="ease-in-out">
					<div class="card-content">
						<figure class="image is-128x128"><img src="{{ asset('/images/pages/home/comup.jpg')}}"
								title="LandSpeed UI"></figure>
						<div class="media-content has-text-centered">
							<h3 class="title is-5 is-uppercase has-text-weight-bold ">
								<a href="/about" class="has-text-dark" title="">
									Clean UI Design</a>
							</h3>
							<p class="is-size-6">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
						</div>
					</div>
				</article>
			</div>
			<div class="column is-one-third">
				<article class="card ls-feature-card" data-aos="flip-left" data-aos-delay="100" data-aos-duration="750"
					data-aos-easing="ease-in-out">
					<div class="card-content">
						<figure class="image is-128x128"><img src="{{ asset('/images/icons/ux.jpg')}}"
								title="LandSpeed UI"></figure>
						</figure>
						<div class="media-content has-text-centered">
							<h3 class="title is-5 is-uppercase has-text-weight-bold"><a href="/services"
									class="has-text-dark" title="">Simple UX</a></h3>
							<p class="is-size-6">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
						</div>
					</div>
				</article>
			</div>
			<div class="column is-one-third">
				<article class="card ls-feature-card" data-aos="flip-left" data-aos-delay="200" data-aos-duration="750"
					data-aos-easing="ease-in-out">
					<div class="card-content">
						<figure class="image is-128x128"><img src="{{ asset('images/icons/speed.jpg')}}"
								title="LandSpeed Fast Performance"></figure>
						<div class="media-content has-text-centered">
							<h3 class="title is-5 is-uppercase has-text-weight-bold"><a href="/portfolio"
									class="has-text-dark" title="">Fast
									Performance</a></h3>
							<p class="is-size-6">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
						</div>
					</div>
				</article>
			</div>
		</div>
		<div class="columns">
			<div class="column is-8-desktop is-offset-2-desktop">
				<div class="ls-section-content has-text-centered">
					<button class="btn type3 has-text-black">
						<a href="/services">Find out more</a>
						<span class="icon is-small">
							<i class="fas fa-arrow-right"></i>
						</span>
					</button>

				</div>
			</div>
		</div>
	</div>
</section>



<section class="section section__portfolio">
	<div class="container">
		<div class="columns">
			<div class="column is-8-desktop is-offset-2-desktop">
				<div class="ls-section-content has-text-centered">
					<h3 class="title is-2 is-uppercase has-text-dark">Portfolio</h3>
				</div>
			</div>
		</div>
		<div class="swiper-container">
			<div class="swiper-wrapper">
				<div class="swiper-slide">
					<div class="container-general">
						<div class="gallery-wrap wrap-effect-1">
							<div class="item-swiper"
								style="background-image: url({{ asset('/images/services/1.png') }})">
								<div class="hover_layer ">
									<p class="hover_text title is-1 is-capitalized has-text-weight-bold">Marketing
										Digital</p>
								</div>
							</div>
							<div class="item-swiper"
								style="background-image: url({{ asset('/images/services/2.png') }})">
								<div class="hover_layer ">
									<p class="hover_text title is-1 is-capitalized has-text-weight-bold">Marketing
										Digital</p>
								</div>
							</div>
							<div class="item-swiper"
								style="background-image: url({{ asset('/images/services/3.gif') }})">
								<div class="hover_layer ">
									<p class="hover_text title is-1 is-capitalized has-text-weight-bold">Marketing
										Digital</p>
								</div>
							</div>
							<div class="item-swiper"
								style="background-image: url({{ asset('/images/services/4.png') }})">
								<div class="hover_layer ">
									<p class="hover_text title is-1 is-capitalized has-text-weight-bold">Marketing
										Digital</p>
								</div>
							</div>
							<div class="item-swiper"
								style="background-image: url({{ asset('/images/services/5.png') }})">
								<div class="hover_layer ">
									<p class="hover_text title is-1 is-capitalized has-text-weight-bold">Marketing
										Digital</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div>
</section>

<section class="section section__blog">
	<div class="container">
		<div class="columns">
			<div class="column is-8-desktop is-offset-2-desktop">
				<div class="ls-section-content has-text-centered">
					<h3 class="title is-2 is-uppercase has-text-dark">Blog</h3>
				</div>
			</div>
		</div>

		<div class="columns is-gapless is-vcentered">
			<div class="column is-one-third-desktop is-full-mobile ">
				<div class="card">
					<div class="card-image">
						<figure class="image is-4by3">
							<img src=" {{ asset('/images/services/1.png') }} " alt="Placeholder image">

						</figure>
						<div class="card-image-overlay has-text-centered">
							<h3 class="title is-2 is-uppercase has-text-black has-text-weight-semibold">
								Test Title
							</h3>
						</div>
					</div>
				</div>
			</div>
			<div class="column is-one-third-desktop is-full-mobile ">
				<div class="card">
					<div class="card-image">
						<figure class="image is-4by3">
							<img src=" {{ asset('/images/services/1.png') }} " alt="Placeholder image">

						</figure>
						<div class="card-image-overlay has-text-centered">
							<h3 class="title is-2 is-uppercase has-text-black has-text-weight-semibold">
								Blog post #2
							</h3>
						</div>
					</div>
				</div>
			</div>
			<div class="column is-one-third-desktop is-full-mobile ">
				<div class="card">
					<div class="card-image">
						<figure class="image is-4by3">
							<img src=" {{ asset('/images/services/1.png') }} " alt="Placeholder image">

						</figure>
						<div class="card-image-overlay has-text-centered">
							<h3 class="title is-2 is-uppercase has-text-black has-text-weight-semibold">
								Big blog post title #3
							</h3>
						</div>
					</div>
				</div>
			</div>

		</div>
</section>

<script>
	AOS.init({
		// Global settings:
		disable: false, // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
		startEvent: 'DOMContentLoaded', // name of the event dispatched on the document, that AOS should initialize on
		initClassName: 'aos-init', // class applied after initialization
		animatedClassName: 'aos-animate', // class applied on animation
		useClassNames: false, // if true, will add content of `data-aos` as classes on scroll
		disableMutationObserver: false, // disables automatic mutations' detections (advanced)
		debounceDelay: 50, // the delay on debounce used while resizing window (advanced)
		throttleDelay: 99, // the delay on throttle used while scrolling the page (advanced)


		// Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
		offset: 120, // offset (in px) from the original trigger point
		delay: 0, // values from 0 to 3000, with step 50ms
		duration: 400, // values from 0 to 3000, with step 50ms
		easing: 'ease', // default easing for AOS animations
		once: true, // whether animation should happen only once - while scrolling down
		mirror: false, // whether elements should animate out while scrolling past them
		anchorPlacement: 'top-bottom', // defines which position of the element regarding to window should trigger the animation

});
</script>

@stop