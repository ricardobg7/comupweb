@extends('frontend.main')

@section('content')

<section class="hero about-hero is-large">
</section>


<section class="section section__about--intro ls-features-cards">
	<div class="container">
		<div class="columns">
			<div class="column is-8-desktop is-offset-2-desktop">
				<div class="ls-section-content has-text-centered">
					<h2 class="title is-2 is-uppercase has-text-dark">About Us</h2>
					<p class="content has-text-grey is-size-5">Small description - Capitalize on low hanging fruit to
						identify a ballpark value added activity to beta test. Override the digital divide
						with additional clickthroughs. This is a brief text followup to the main page
					</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section section__about--first">
	<div class="container">
		<div class="columns">
			<div class="column is-half is-full-mobile u-vcenter">
				<figure class="image about__image">
					<img class="u-flip-image" src="{{ asset('/images/pages/about/data.png')}}">
				</figure>
			</div>
			<div class="column is-half is-full-mobile u-vcenter">
				<div class="has-text-centered">
					<h2 class="title is-2 is-uppercase has-text-dark">Something Important</h2>
					<p class="content has-text-grey is-size-5">Small description - Capitalize on low hanging fruit to
						identify a ballpark value added activity to beta test. Override the digital divide
						with additional clickthroughs. This is a brief text followup to the main page
					</p>
				</div>
			</div>
		</div>

		<div class="columns columns--flip">

			<div class="column is-half is-full-mobile u-vcenter">
				<div class="has-text-centered">
					<h2 class="title is-2 is-uppercase has-text-dark">Something Important</h2>
					<p class="content has-text-grey is-size-5">Small description - Capitalize on low hanging fruit to
						identify a ballpark value added activity to beta test. Override the digital divide
						with additional clickthroughs. This is a brief text followup to the main page
					</p>
				</div>
			</div>
			<div class="column is-half is-full-mobile u-vcenter">
				<figure class="image about__image">
					<img src="{{ asset('/images/pages/about/data.png')}}">
				</figure>
			</div>
		</div>
	</div>
</section>




<section class="section section__about--more">
	<div class="container">
		<div class="columns is-centered">

			<div class="column is-one-quarter-desktop is-full-mobile has-text-centered">
				<h3 class="title is-4 is-uppercase has-text-black has-text-weight-semibold">
					Check our Services
				</h3>

				<button class="btn type3 has-text-black">
					<a href="/services">redirect #1</a>
					<span class="icon is-small">
						<i class="fas fa-arrow-right"></i>
					</span>
				</button>

			</div>
			<div class="column is-one-quarter-desktop is-full-mobile has-text-centered">
				<h3 class="title is-4 is-uppercase has-text-black has-text-weight-semibold">
					Check our Portfolio
				</h3>

				<button class="btn type3 has-text-black">
					<a href="/services">redirect #2</a>
					<span class="icon is-small">
						<i class="fas fa-arrow-right"></i>
					</span>
				</button>

			</div>
			<div class="column is-one-quarter-desktop is-full-mobile has-text-centered">
				<h3 class="title is-4 is-uppercase has-text-black has-text-weight-semibold">
					Check our Blog
				</h3>

				<button class="btn type3 has-text-black">
					<a href="/services">redirect #3</a>
					<span class="icon is-small">
						<i class="fas fa-arrow-right"></i>
					</span>
				</button>

			</div>
		</div>
	</div>

</section>
@stop