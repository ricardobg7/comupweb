@extends('frontend.main')

@section('content')

<section class="hero about-hero is-large">
</section>

<section class="section section__about--intro ls-features-cards">
	<div class="container">
		<div class="columns">
			<div class="column is-8-desktop is-offset-2-desktop">
				<div class="ls-section-content has-text-centered">
					<h2 class="title is-3 is-uppercase has-text-dark">Contact Us</h2>
					<p class="content has-text-grey is-size-6">Small description about us / to contact us - Capitalize
						on low hanging fruit to
						identify a ballpark value added activity to beta test. Override the digital divide
						with additional clickthroughs. This is a brief text followup to the main page
					</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section  contact">
	<div class="container">
		<form id="form" name="contact" method="POST"><input type="hidden" name="form-name" value="contact">
			<div class="columns is-centered">
				<p class="hidden is-hidden">
					<label>Don’t fill this out if you're human: <input name="bot-field"></label>
				</p>
				<div class="column is-half">
					<div class="field">
						<label class="label">Name</label>
						<div class="control is-expanded">
							<input class="input is-medium" name="name" id="name" type="text" required="">
						</div>
					</div>
				</div>
				<div class="column is-half">
					<div class="field">
						<label class="label">Email</label>
						<div class="control is-expanded">
							<input class="input is-medium" type="email" id="email" name="email" required="">
						</div>
					</div>
				</div>
			</div>
			<div class="columns is-centered">
				<div class="column">
					<div class="field">
						<label class="label">Message</label>
						<div class="control is-expanded">
							<textarea class="textarea is-medium" name="comment" id="comment" rows="5"
								required=""></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="columns is-centered">
				<div class="column is-one-third">
					<div class="field">
						<div class="control">
							<button class="button is-medium is-rounded" type="submit">Submit</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</section>

@stop