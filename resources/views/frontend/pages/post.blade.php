@extends('frontend.main')

@section('content')



<section class="section section__about--first">
	<div class="container">
		<div class="columns is-multiline is-mobile">
			<div class="column is-8-desktop is-offset-2-desktop is-full-mobile u-vcenter">
				<figure class="image about__image">
					<img src="{{ asset('/images/pages/about/data.png')}}">
				</figure>
			</div>
			<div class="column is-8-desktop is-offset-2-desktop">
				<div class="ls-section-content has-text-centered">
					<h2 class="title is-2 is-uppercase has-text-dark">John Smith</h2>
					<p class="content has-text-grey is-size-5">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						Phasellus nec iaculis mauris. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						Phasellus nec iaculis mauris.
						Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						Phasellus nec iaculis mauris.
						Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						Phasellus nec iaculis mauris.
						Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						Phasellus nec iaculis mauris.
						Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						Phasellus nec iaculis mauris.
						Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						Phasellus nec iaculis mauris.<a>@bulmaio</a>.
					</p>

					<br>
					<p class="content has-text-grey is-size-5">
						<a href="#">#css</a> <a href="#">#responsive</a>
					</p>
					<br>
					<p class="content has-text-grey is-size-5">

						<time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
					</p>
				</div>
			</div>

		</div>
	</div>
</section>

@stop