@extends('frontend.main')

@section('content')

<section class="hero about-hero is-large">
</section>

<section class="section section__about--intro ls-features-cards">
	<div class="container">
		<div class="columns">
			<div class="column is-8-desktop is-offset-2-desktop">
				<div class="ls-section-content has-text-centered">
					<h2 class="title is-3 is-uppercase has-text-dark">Our Services</h2>
					<p class="content has-text-grey is-size-6">Small description - Capitalize on low hanging fruit to
						identify a ballpark value added activity to beta test. Override the digital divide
						with additional clickthroughs. This is a brief text followup to the main page
					</p>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="section section__about--first">
	<div class="container">
		<div class="columns">
			<div class="column is-half is-full-mobile u-vcenter">
				<figure class="image about__image">
					<img class="u-flip-image" src="{{ asset('/images/pages/about/data.png')}}">
				</figure>
			</div>
			<div class="column is-half is-full-mobile u-vcenter">
				<div class="has-text-centered">
					<h2 class="title is-3 is-uppercase has-text-dark">Design</h2>
					<p class="content has-text-grey is-size-6">Small description - Capitalize on low hanging fruit to
						identify a ballpark value added activity to beta test. Override the digital divide
						with additional clickthroughs. This is a brief text followup to the main page
					</p>
				</div>
			</div>
		</div>

		<div class="columns columns--flip">

			<div class="column is-half is-full-mobile u-vcenter">
				<div class="has-text-centered">
					<h2 class="title is-3 is-uppercase has-text-dark">Web Dev</h2>
					<p class="content has-text-grey is-size-6">Small description - Capitalize on low hanging fruit to
						identify a ballpark value added activity to beta test. Override the digital divide
						with additional clickthroughs. This is a brief text followup to the main page
					</p>
				</div>
			</div>
			<div class="column is-half is-full-mobile u-vcenter">
				<figure class="image about__image">
					<img src="{{ asset('/images/pages/about/data.png')}}">
				</figure>
			</div>
		</div>

		<div class="columns">
			<div class="column is-half is-full-mobile u-vcenter">
				<figure class="image about__image">
					<img class="u-flip-image" src="{{ asset('/images/pages/about/data.png')}}">
				</figure>
			</div>
			<div class="column is-half is-full-mobile u-vcenter">
				<div class="has-text-centered">
					<h2 class="title is-3 is-uppercase has-text-dark">Digital Marketing</h2>
					<p class="content has-text-grey is-size-6">Small description - Capitalize on low hanging fruit to
						identify a ballpark value added activity to beta test. Override the digital divide
						with additional clickthroughs. This is a brief text followup to the main page
					</p>
				</div>
			</div>
		</div>

		<div class="columns columns--flip">

			<div class="column is-half is-full-mobile u-vcenter">
				<div class="has-text-centered">
					<h2 class="title is-3 is-uppercase has-text-dark">Festas surpresa</h2>
					<p class="content has-text-grey is-size-6">Small description - Capitalize on low hanging fruit to
						identify a ballpark value added activity to beta test. Override the digital divide
						with additional clickthroughs. This is a brief text followup to the main page
					</p>
				</div>
			</div>
			<div class="column is-half is-full-mobile u-vcenter">
				<figure class="image about__image">
					<img src="{{ asset('/images/pages/about/data.png')}}">
				</figure>
			</div>
		</div>

		<div class="columns is-multiline is-centered column-box">
			<div class="column is-full-desktop ">
				<div class="ls-section-content has-text-centered">
					<h3 class="title is-3 is-uppercase has-text-dark">Box Moments</h3>
					<p class="content has-text-grey is-size-6">Small description - Capitalize on low hanging fruit to
						identify a ballpark value added activity to beta test.
					</p>
				</div>
			</div>
			<div class="column is-half-desktop is-offset-0-desktop is-full-mobile u-vcenter">
				<figure class="image boxmoments-figure has-text-centered">
					<a href="https://box-moments.com" target="_blank">
						<img class="u-flip-image" src="{{ asset('/images/pages/about/boxmoments.png')}}">
					</a>
				</figure>
			</div>
		</div>

	</div>
</section>

@stop