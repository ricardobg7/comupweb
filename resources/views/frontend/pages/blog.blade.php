@extends('frontend.main')

@section('content')

<section class="hero about-hero is-large">
</section>


<section class="section section__about--intro ls-features-cards">
	<div class="container">
		<div class="columns">
			<div class="column is-8-desktop is-offset-2-desktop">
				<div class="ls-section-content has-text-centered">
					<h2 class="title is-3 is-uppercase has-text-dark">Blog Title</h2>
					<p class="content has-text-grey is-size-6">

						Small description about us / to contact us - Capitalize on low hanging fruit to
						identify a ballpark value added activity to beta test. Override the digital divide
						with additional clickthroughs. This is a brief text followup to the main page

					</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section section__about--first">
	<div class="container">
		<div class="columns is-multiline is-mobile">
			<div class="column is-one-third is-full-mobile u-vcenter">
				<div class="card">
					<div class="card-image">
						<figure class="image " style="padding: 2.5rem; ">
							<img src="{{ asset('/images/pages/about/data.png')}}" alt="Placeholder image">

						</figure>
					</div>
					<div class="card-content">
						<div class="media">
							<div class="media-content">
								<a href="/blog/post">
									<p class="title is-4">John Smith</p>
								</a>
							</div>
						</div>

						<div class="content">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit.
							Phasellus nec iaculis mauris. <a>@bulmaio</a>.
							<br>
							<a href="#">#css</a> <a href="#">#responsive</a>
							<br>
							<a href="/blog/post">
								<time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
							</a>
						</div>
					</div>
				</div>
			</div>

			<div class="column is-one-third is-full-mobile u-vcenter">
				<div class="card">
					<div class="card-image">
						<figure class="image " style="padding: 2.5rem; ">
							<img src="{{ asset('/images/pages/about/spreadsheet.png')}}" alt="Placeholder image">

						</figure>
					</div>
					<div class="card-content">
						<div class="media">
							<div class="media-content">
								<a href="#">
									<p class="title is-4">John Smith</p>
								</a>
							</div>
						</div>

						<div class="content">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit.
							Phasellus nec iaculis mauris. <a>@bulmaio</a>.
							<br>
							<a href="#">#css</a> <a href="#">#responsive</a>
							<br>
							<a href="#">
								<time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="column is-one-third is-full-mobile u-vcenter">
				<div class="card">
					<div class="card-image">
						<figure class="image ">
							<img src=" {{ asset('/images/pages/about/data.png')}}" alt="Placeholder image">
						</figure>
					</div>
					<div class="card-content">
						<div class="media">
							<div class="media-content">
								<a href="#">
									<p class="title is-4">John Smith</p>
								</a>
							</div>
						</div>

						<div class="content">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit.
							Phasellus nec iaculis mauris. <a>@bulmaio</a>.
							<br>
							<a href="#">#css</a> <a href="#">#responsive</a>
							<br>
							<a href="#">
								<time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
							</a>
						</div>
					</div>
				</div>
			</div>

			<div class="column is-one-third-desktop is-full-mobile u-vcenter">
				<div class="card">
					<div class="card-image">
						<figure class="image " style="padding: 2.5rem; ">
							<img src="{{ asset('/images/pages/about/data.png')}}" alt="Placeholder image">

						</figure>
					</div>
					<div class="card-content">
						<div class="media">
							<div class="media-content">
								<a href="#">
									<p class="title is-4">John Smith</p>
								</a>
							</div>
						</div>

						<div class="content">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit.
							Phasellus nec iaculis mauris. <a>@bulmaio</a>.
							<br>
							<a href="#">#css</a> <a href="#">#responsive</a>
							<br>
							<a href="#">
								<time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
							</a>
						</div>
					</div>
				</div>
			</div>

			<div class="column is-one-third is-full-mobile u-vcenter">
				<div class="card">
					<div class="card-image">
						<figure class="image " style="padding: 2.5rem; ">
							<img src="{{ asset('/images/pages/about/data.png')}}" alt="Placeholder image">
						</figure>
					</div>
					<div class="card-content">
						<div class="media">
							<div class="media-content">
								<a href="#">
									<p class="title is-4">John Smith</p>
								</a>

							</div>
						</div>

						<div class="content">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit.
							Phasellus nec iaculis mauris. <a>@bulmaio</a>.
							<br>
							<a href="#">#css</a> <a href="#">#responsive</a>
							<br>
							<a href="#">
								<time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
							</a>
						</div>
					</div>
				</div>
			</div>

			<div class="column is-one-third is-full-mobile u-vcenter">
				<div class="card">
					<div class="card-image">
						<figure class="image " style="padding: 2.5rem; ">
							<img src="{{ asset('/images/pages/about/data.png')}}" alt="Placeholder image">
						</figure>
					</div>
					<div class="card-content">
						<div class="media">
							<div class="media-content">
								<a href="#">
									<p class="title is-4">John Smith</p>
								</a>

							</div>
						</div>

						<div class="content">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit.
							Phasellus nec iaculis mauris. <a>@bulmaio</a>.
							<br>
							<a href="#">#css</a> <a href="#">#responsive</a>
							<br>
							<a href="#">
								<time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop