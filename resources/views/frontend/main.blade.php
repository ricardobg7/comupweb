@include('frontend._partials._head')
<header>
	@include('frontend._partials._header')
</header>

<body>
	<div id="loader" class="pageloader ">

		<span class="title is-white">Loading</span>
	</div>

	@yield('content')


</body>

@include('frontend._partials._footer')